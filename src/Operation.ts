import OperationType, { IOperationType } from './OperationType';
import { ILinePojo } from './Processor';

export interface IOperation extends Object {
  type: string;
  info: IOperationType;
  types: IOperationType;
}

export default class Operation implements IOperation {
  public type: string = '';
  public info: IOperationType;
  public types: IOperationType;

  constructor(params: ILinePojo) {
    this.type = params.operationType;
    this.info = this.setInfoData(params);
    this.types = this.setTypes(params);
  }

  /**
   * PRIVATE METHODS
   */
  private setInfoData(params: ILinePojo) {
    return new OperationType(params);
  }

  private setTypes(params: ILinePojo) {
    return {
      ...this.types,
      [params.operation]: new OperationType(params),
    };
  }

  private static existsInCurrentState(typesState: IOperationType, operationType: string) {
    return Object.keys(typesState).includes(operationType);
  }

  private static addEntry(currentState: IOperation, newData: ILinePojo) {
    const newInfo = new OperationType(newData);
    const newType = new OperationType(newData);

    return {
      ...currentState,
      info: newInfo.updateValues(currentState.info),
      types: {
        ...currentState.types,
        [newData.operation]: {
          ...newType,
          totalCount: 1,
        },
      }
    };
  }

  private static transformCurrentState(currentState: IOperation, newData: ILinePojo) {
    const newInfo = new OperationType(newData);
    const newType = new OperationType(newData);

    return {
      ...currentState,
      info: newInfo.updateValues(currentState.info),
      types: {
        ...currentState.types,
        [newData.operation]: newType.updateValues(currentState.types[newData.operation]),
      }
    };
  }

  /**
   * PUBLIC METHODS
   */
  public static handleNewData(currentState: IOperation, newData: ILinePojo) {
    if (Operation.existsInCurrentState(currentState.types, newData.operation)) {
      return Operation.transformCurrentState(currentState, newData);
    } else {
      return Operation.addEntry(currentState, newData);
    }
  }
}
