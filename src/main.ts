import App from './App';
import StateService from './StateService';

async function main() {
  StateService.build();
  const app = await App.build();
  app.outputLogs();
}

main();
