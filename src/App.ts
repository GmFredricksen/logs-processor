import FileReader from './FileReader';
import Presenter from './Presenter';
import Processor from './Processor';
import StateService from './StateService';
import Operation, { IOperation } from './Operation';

export default class App {
  private fileReader?: FileReader;
  private fileDataProcessor?: Processor;
  private outputPresenter?: Presenter;
  private static instance: App;

  /**
   * PRIVATE METHODS
   */
  private static getInstance() {
    return App.instance;
  }

  private async initialize() {
    try {
      this.fileReader = new FileReader({ file: 'logs.log', directory: 'logs' });
      const rlOfStream = await this.fileReader.getReadLineOfStream();
      this.fileDataProcessor = new Processor({ rlOfStream });
      this.outputPresenter = new Presenter();

      return this;
    } catch (error) {
      throw new Error('App initialize -> ' + error.message);
    }
  }

  private async readFileLines() {
    const content = await this.fileDataProcessor?.parseLineByLine();

    return content;
  }

  /**
   * PUBLIC METHODS
   */
  public static async build() {
    if (!App.getInstance()) {
      App.instance = await new App().initialize();
    }

    return App.instance;
  }

  public async outputLogs() {
    await this.readFileLines();
    const state = StateService.getServiceContainer();
    const stateObjects: any = state.mget(state.keys());
    const arrayOfStateObjects: IOperation[] = state.keys().map((key) => {
      return stateObjects[key];
    });

    this.outputPresenter?.showLogs(arrayOfStateObjects);
  }
}
