import { ILinePojo } from './Processor';

export interface IOperationType extends Object {
  totalCount: number;
  totalDuration: number;
  averageDuration: number;
  maxDuration: number;
  minDuration: number;
}

export default class OperationType implements IOperationType {
  public totalCount: number = 0;
  public totalDuration: number = 0;
  public averageDuration: number = 0;
  public maxDuration: number = 0;
  public minDuration: number = 0;

  constructor(params: ILinePojo) {
    this.totalCount = 1;
    this.totalDuration = Number(params.duration);
    this.averageDuration = Number(params.duration);
    this.maxDuration = Number(params.duration);
    this.minDuration = Number(params.duration);
  }

  /**
   * PRIVATE METHODS
   */
  private checkMax(prevMaxDuration: number) {
    if (prevMaxDuration > this.maxDuration) {
      this.maxDuration = prevMaxDuration;
    }
  }

  private checkMin(prevMinDuration: number) {
    if (prevMinDuration < this.minDuration) {
      this.minDuration = prevMinDuration;
    }
  }

  /**
   * PUBLIC METHODS
   */
  public updateValues(prevState: IOperationType) {
    this.totalCount += prevState.totalCount;
    this.totalDuration += prevState.totalDuration;
    this.averageDuration = this.totalDuration / this.totalCount;
    this.checkMax(prevState.maxDuration);
    this.checkMin(prevState.minDuration);

    return this;
  }
}
