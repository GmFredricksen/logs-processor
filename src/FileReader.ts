import fs, { promises } from 'fs';
import readline from 'readline';

interface IFileReaderParams {
  file: string;
  directory: string;
}

class FileReader {
  private fullPathToFile;

  constructor({ ...params }: IFileReaderParams) {
    this.fullPathToFile = `${__dirname}/${params.directory}/${params.file}`;
  }

  public async getReadLineOfStream() {
    const fileStream = fs.createReadStream(this.fullPathToFile);

    const rlOfStream = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });

    return rlOfStream;
  }
}

export default FileReader;
