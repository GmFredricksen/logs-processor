import NodeCache from 'node-cache';
import { ILinePojo } from './Processor';
import Operation from './Operation';

export default class StateService {
  private static cache: NodeCache;

  /**
   * PUBLIC METHODS
   */
  public static addEntry(newEntry: Operation): boolean {
    return StateService.cache.set(newEntry.type, newEntry);
  }

  public static entryTypeExists(key: string): boolean {
    return StateService.cache.has(key);
  }

  public static async build() {
    if (!StateService.getServiceContainer()) {
      StateService.cache = new NodeCache();
    }

    return StateService.cache;
  }

  public static getServiceContainer() {
    return StateService.cache;
  }

  public static updateEntry(entryToUpdate: ILinePojo) {
    const operationCurrentState: any = StateService.cache.get(entryToUpdate.operationType);

    return StateService.cache.set(
      entryToUpdate.operationType,
      Operation.handleNewData(operationCurrentState, entryToUpdate)
    );
  }
}
