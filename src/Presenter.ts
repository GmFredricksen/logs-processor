import { IOperation } from './Operation';

export default class Presenter {
  public showLogs(types: IOperation[]) {
    types.forEach((operation) => this.generateOutput(operation));
  }

  private generateOutput(operation: IOperation): void {
    // tslint:disable-next-line:quotemark
    console.log("\n\n");
    console.group(operation.type);
    console.table(operation.info);
    console.table(operation.types, ['totalCount', 'averageDuration', 'maxDuration', 'minDuration']);
    console.groupEnd();
  }
}
