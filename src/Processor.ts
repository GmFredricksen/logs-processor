import readline from 'readline';
import Operation from './Operation';
import StateService from './StateService';

interface IProcessorParams {
  rlOfStream: readline.Interface;
}

export interface ILinePojo extends Object {
  duration: string;
  operation: string;
  operationType: string;
}

class Processor {
  private rlOfStream;

  public operationTypes: string[] = [];

  constructor({ ...params }: IProcessorParams) {
    this.rlOfStream = params.rlOfStream;
  }

  /**
   * PRIVATE METHODS
   */
  private addOperationTypeEntry(operationType: string): number {
    return this.operationTypes.push(operationType);
  }

  private addOperationToState(linePojo: ILinePojo) {
    StateService.addEntry(new Operation(linePojo));
  }

  private processLine(line: string): ILinePojo | undefined {
    const splittedLine: string[] = line.split('|');
    const logEntryType = splittedLine.shift()?.trim();

    if (logEntryType !== '[graphql] operation-responsetime') return undefined;

    const linePojo: ILinePojo = splittedLine.reduce((aggregator, currentValue) => {
      const elOfLine: string[] = currentValue.trim().split(':');

      return {
        [elOfLine[0]]: elOfLine[1]?.trim(),
        ...aggregator,
      };
    }, {});

    return linePojo;
  }

  private handleOperationType(linePojo: ILinePojo) {
    if (
      !this.operationTypes.includes(linePojo.operationType)
      && !StateService.entryTypeExists(linePojo.operationType)
    ) {
      this.addOperationTypeEntry(linePojo.operationType);
      this.addOperationToState(linePojo);
    } else {
      this.updateOperationState(linePojo);
    }

    return true;
  }

  private updateOperationState(linePojo: ILinePojo) {
    StateService.updateEntry(linePojo);
  }

  /**
   * PUBLIC METHODS
   */
  public async parseLineByLine(): Promise<object> {
    let mainObj: object = {};

    for await (const line of this.rlOfStream) {
      const linePojo = this.processLine(line);

      if (linePojo !== undefined) {
        this.handleOperationType(linePojo);
      }
    }

    return mainObj;
  }
}

export default Processor;
