export const generalInfo = {
  totalOperations: 1000,
  totalQueries: 600,
  totalMutations: 300,
  totalSubscriptions: 100,
};

export const queries = {
  type: 'Query',
  info: {
    totalCount: 39,
    totalDuration: 3600,
    averageDuration: 6,
    maxDuration: 10,
    minDuration: 2,
  },
  types: {
    SearchPatients: {
      totalCount: 13,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2,
    },
    GetExporterTemplates: {
      totalCount: 13,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2,
    },
    GetExporterObjects: {
      totalCount: 13,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2,
    },
  },
};

export const mutations = {
  type: 'Mutation',
  info: {
    totalCount: 39,
    totalDuration: 3600,
    averageDuration: 6,
    maxDuration: 10,
    minDuration: 2,
  },
  types: {
    updatePatient: {
      totalCount: 13,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2
    },
    stopListeningForConnectorClients: {
      totalCount: 23,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2
    },
    createCase: {
      totalCount: 13,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2
    }
  }
};

export const subscriptions = {
  type: 'Subscription',
  info: {
    totalCount: 39,
    totalDuration: 3600,
    averageDuration: 6,
    maxDuration: 10,
    minDuration: 2,
  },
  types: {
    onCaseEvent: {
      totalCount: 13,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2
    },
    onDigitalConsultationEvent: {
      totalCount: 23,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2
    },
    onExportFinished: {
      totalCount: 13,
      totalDuration: 1200,
      averageDuration: 6,
      maxDuration: 10,
      minDuration: 2
    }
  }
};
