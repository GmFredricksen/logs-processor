# Logs Processor

Sample implementation of a JS parser of log files.

## Content

* [Before you start](#before-you-start)
* [How to run the application](#how-to-run-the-application)
* [TODO](#todo)

## Before you start

Make sure you have the following tools available in your system:

* [NodeJS](https://nodejs.org/en/) _(tested with v14.13.0)_
* [Yarn](https://yarnpkg.com/) _(tested with v1.22.10)_
* [Typescript](https://www.typescriptlang.org/) _(tested with v4.1.2)_

## How to run the application

Make sure **[start-script](./start-script)** is executable

```bash
# if needed run
chmod +x start-script
```

Ideally you should be able to run:

```bash
# 1. install dependencies
yarn

# 2. run the application
yarn start
```

## TODO

* Implement persistence of operations state into Database table
* Add unit tests
* Dynamically parse all files in the `./logs` folder
* Dynamically check if input data from file or `stdin`
* Dynamically watch for changes in input: file or `stdin` and keep process running until closed
* Add `Dockerfile` for deployment
